package logica;

import java.io.Serializable;

public class materia implements Serializable{
	
		private int id;
		private int creditos;
		private String nombre;
		
		public int getId() {
			return id;
		}
		public void Id(int id) {
			this.id = id;
		}
		
		public int getCreditos() {
			return creditos;
		}
		public void  Creditos(int creditos) {
			this.creditos = creditos;
			}
		public String getNombre() {
			return nombre;
		}



		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		
		
		


		
		public materia(int id, int creditos, String nombre) {
			this.id = id;
			this.creditos = creditos;
			this.nombre = nombre;
			
		}
		
		@Override
		public String toString() {
			return this.id + "\t" + this.nombre + "\t" + this.creditos;
		}

}
