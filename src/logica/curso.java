package logica;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import persistencia.ArchivoSerializable;



public class curso{

	private HashMap<Integer, materia> materias;
	private Object materia1;

	public HashMap<Integer, materia> getMaterias() {
		return materias;
	}



	public void crearmateria(int id , String nombre,int creditos) {
		materia materias= new materia(id, creditos, nombre);
		this.crearmateria(id, nombre, creditos);

	}
	public void almacenarTP() throws Exception{
		ArrayList<String> lineasmateria = new ArrayList<String>();
		for(Integer id : this.materias.keySet()) {
			materia materia1 = this.materias.get(id);
			lineasmateria.add(materia1.getId() + ";" + materia1.getNombre() + ";" + materia1.getCreditos());
		}
		ArchivoSerializable.almacenar("materia.csv", lineasmateria);



	}
}
